package com.user.registration.controller;

import com.user.registration.entity.User;
import com.user.registration.service.IUserService;
import com.user.registration.service.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/user-registration")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    private final IUserService service;

    public UserController(IUserService service) {
        this.service = service;
    }

    @PostMapping("/save")
    public Mono<User> create(@Valid @RequestBody User user){
       return service.create(user);

    }

    @GetMapping("/findUser/{userId}")
    public Mono<User> findByUserId(@PathVariable String userId){
      return service.findByUserId(userId);
    }

    @GetMapping("/getAllUsers")
    public Flux<User> findUserAccounts()
    {
        return service.findAllUsers();
   }

    @GetMapping("/test")
    public Mono<String> test() {
        return Mono.just("Microservice-running");
    }

}
