package com.user.registration.service;

import com.user.registration.entity.User;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IUserService {

    Mono<User> create(User user);

    Mono<User> findByUserId(String userId);

    Flux<User> findAllUsers();
}
