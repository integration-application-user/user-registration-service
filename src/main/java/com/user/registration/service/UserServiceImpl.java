package com.user.registration.service;

import com.user.registration.entity.User;
import com.user.registration.exceptions.NotFoundException;
import com.user.registration.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class UserServiceImpl implements IUserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    private final UserRepository repository;

    private final KafkaTemplate<String, User> kafkaTemplate;

    public UserServiceImpl(UserRepository repository,
                           KafkaTemplate<String, User> kafkaTemplate) {
        this.repository = repository;
        this.kafkaTemplate = kafkaTemplate;
    }

    @Override
    public Mono<User> create(User user) {
        fireUserCreatedEvent(user);
        return repository.save(user)
                .doOnSubscribe(i -> LOGGER.info("process complete {}", i));


    }

    @Override
    public Mono<User> findByUserId(String userId) {
        return repository.findById(userId)
                         .switchIfEmpty(Mono.error(new NotFoundException("User not found")))
                         .doOnNext(s -> LOGGER.info("User id found {}", s))
                         .doOnSubscribe(i -> LOGGER.info("process complete {}", i));

    }

    @Override
    public Flux<User> findAllUsers() {
        return repository.findAll()
                         .doOnSubscribe(i -> LOGGER.info("process complete {}", i));
    }

    private void  fireUserCreatedEvent(User user) {
        kafkaTemplate.send("user", user.getUserId() + "created", user);
    }
}
