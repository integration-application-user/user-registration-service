package com.user.registration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;


@SpringBootApplication
public class UserRegistrationApplication {
	public static void main(String[] args) {

        SpringApplication.run(UserRegistrationApplication.class, args);
        System.out.println("available procesor"+Runtime.getRuntime().availableProcessors());
	}

}
