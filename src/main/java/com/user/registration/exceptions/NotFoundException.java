package com.user.registration.exceptions;

public class NotFoundException extends  RuntimeException {
    public NotFoundException(String s)
    {
        super(s);
    }
}
